'use strict';

let index = 1;
const alphabet = [
  '_', 'a', 'b', 'c', 'd', 'e', 'f', '_', 'g', 'h', 'i', 'j',
  'k', 'l', 'm', 'n', '_', 'o','p', 'r', 's', 't', 'u', 'w',
  'x', 'y', 'z', '<-'
];

const stage = document.getElementById('stage');
const result = document.getElementById('text');
result.textContent = '';

function showElement(i, elements) {
  stage.textContent = elements[i];
}

// const interval = setInterval(function() {
//   index += 1;
//   showElement(index, alphabet);
//
//
//   if (index >= alphabet.length - 1) {
//     index = -1;
//   }
// }, 1000);

const throttle = (func, limit) => {
  let lastFunc
  let lastRan
  return function() {
    const context = this
    const args = arguments
    if (!lastRan) {
      func.apply(context, args)
      lastRan = Date.now()
    } else {
      clearTimeout(lastFunc)
      lastFunc = setTimeout(function() {
        if ((Date.now() - lastRan) >= limit) {
          func.apply(context, args)
          lastRan = Date.now()
        }
      }, limit - (Date.now() - lastRan))
    }
  }
}

function react(e) {
  e.preventDefault();

  // ->
  if (e && e.keyCode === 39) {
    index += 1;
    showElement(index, alphabet);


    if (index >= alphabet.length - 1) {
      index = -1;
    }

    return;
  }

  // <-
  if (e && e.keyCode === 37) {
    index -= 1;
    showElement(index, alphabet);


    if (index <= 0) {
      index = alphabet.length - 1;
    }

    return;
  }

  // del
  if (e && e.keyCode === 8) {
    result.innerHTML = '';
    return;
  }

  // up or down
  if (e && e.keyCode === 38 || e.keyCode === 40) {
    return;
  }

  let symbol = alphabet[index];
  if (symbol) {
console.log(e.keyCode);
    if (symbol === '_') {
      symbol = '&nbsp;';
    }

    if (symbol === '<-') {
      result.innerHTML = '';
      return;
    }

    result.innerHTML += symbol;
  }
}

// window.ontouchstart = throttle(react, 800);
window.onkeyup = react;
// window.onclick = throttle(react, 800);
showElement(index, alphabet);
